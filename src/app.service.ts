import { Injectable } from '@nestjs/common';
import { BudgetServiceClient } from '@google-cloud/billing-budgets';

@Injectable()
export class AppService {
  // 01E906-16A8E9-D66704
  async listBudgets(billingAccount = 'billingAccounts/013F81-84C2F0-FE0B90') {
    const budget = new BudgetServiceClient(); // credentials à préciser dans le constructeur ? dans l'env ?
    const result = await budget.listBudgets({
      parent: billingAccount,
    });
    console.log(result);
    return result;
  }
}
